package com.bakaeva.tm.api.service;

import com.bakaeva.tm.dto.Domain;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;

public interface IDomainService {

    void load(@Nullable Domain domain);

    void save(@Nullable Domain domain);

    boolean saveToXml();

    boolean loadFromXml();

    boolean removeXml();

    boolean saveToJson();

    boolean loadFromJson();

    boolean removeJson();

    boolean saveToBinary();

    boolean loadFromBinary();

    boolean removeBinary();

    boolean saveToBase64();

    @SneakyThrows
    boolean loadFromBase64();

    boolean removeBase64();

}