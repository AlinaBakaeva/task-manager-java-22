package com.bakaeva.tm.api.repository;

import com.bakaeva.tm.api.IRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import com.bakaeva.tm.entity.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findById(@NotNull String id);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User removeById(@NotNull String id);

    @Nullable
    User removeByLogin(@NotNull String login);
    
}