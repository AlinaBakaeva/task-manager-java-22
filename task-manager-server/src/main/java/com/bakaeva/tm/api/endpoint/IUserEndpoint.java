package com.bakaeva.tm.api.endpoint;

import com.bakaeva.tm.entity.Session;
import com.bakaeva.tm.entity.User;
import com.bakaeva.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IUserEndpoint {

    @Nullable
    @WebMethod
    User createUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password
    );

    @NotNull
    @WebMethod
    User createUserWithEmail(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password,
            @WebParam(name = "email") @Nullable String email
    );

    @NotNull
    @WebMethod
    User registerUserWithEmail(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password,
            @WebParam(name = "email") @Nullable String email
    );

    @NotNull
    @WebMethod
    User createUserWithRole(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password,
            @WebParam(name = "role") @Nullable Role role
    );

    @Nullable
    @WebMethod
    User findUserById(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String id
    );

    @Nullable
    @WebMethod
    User findUserByLogin(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "login") @Nullable String login
    );

    @Nullable
    @WebMethod
    User removeUserById(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String id
    );

    @Nullable
    @WebMethod
    User removeUserByLogin(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "login") @Nullable String login
    );

    @NotNull
    @WebMethod
    User updateUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "firstName") @Nullable String firstName,
            @WebParam(name = "lastName") @Nullable String lastName,
            @WebParam(name = "middleName") @Nullable String middleName,
            @WebParam(name = "email") @Nullable String email
    );

    @NotNull
    @WebMethod
    User updateUserById(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String id,
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "firstName") @Nullable String firstName,
            @WebParam(name = "lastName") @Nullable String lastName,
            @WebParam(name = "middleName") @Nullable String middleName,
            @WebParam(name = "email") @Nullable String email
    );

    @NotNull
    @WebMethod
    User updateUserPassword(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "password") @Nullable String password
    );

    @NotNull
    @WebMethod
    User updateUserPasswordById(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String id,
            @WebParam(name = "password") @Nullable String password
    );

    @Nullable
    @WebMethod
    User lockUserByLogin(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "login") @Nullable String login
    );

    @Nullable
    @WebMethod
    User unlockUserByLogin(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "login") @Nullable String login
    );

    @NotNull
    @WebMethod
    List<User> findUserAll(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    void removeUserAll(
            @WebParam(name = "session") @Nullable Session session
    );

    @Nullable
    @WebMethod
    User removeUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user
    );

}
