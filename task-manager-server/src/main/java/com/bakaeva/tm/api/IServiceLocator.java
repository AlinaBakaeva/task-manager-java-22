package com.bakaeva.tm.api;

import com.bakaeva.tm.api.service.*;
import org.jetbrains.annotations.NotNull;

public interface IServiceLocator {

    @NotNull
    IUserService getUserService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ISessionService getSessionService();

}