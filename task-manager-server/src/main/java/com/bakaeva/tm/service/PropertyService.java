package com.bakaeva.tm.service;

import com.bakaeva.tm.api.service.IPropertyService;
import com.bakaeva.tm.constant.PropertyConstant;
import com.bakaeva.tm.exception.incorrect.IncorrectPropertyFileException;
import com.bakaeva.tm.exception.system.NoSuchPropertyException;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull
    private final Properties properties = new Properties();

    @Override
    public void init() {
        @NotNull final String path = PropertyConstant.FILE_APPLICATION_PROPERTIES;
        try (@Nullable final InputStream inputStream = PropertyService.class.getResourceAsStream(path)) {
            properties.load(inputStream);
        } catch (IOException e) {
            throw new IncorrectPropertyFileException(path.substring(1));
        }
    }

    @Override
    @NotNull
    public String getServerHost() {
        @Nullable final String propertyHost = properties.getProperty(PropertyConstant.SERVER_HOST);
        @Nullable final String envHost = System.getProperty(PropertyConstant.SERVER_HOST);
        if (envHost != null) return envHost;
        if (propertyHost == null) throw new NoSuchPropertyException(PropertyConstant.SERVER_HOST);
        return propertyHost;
    }

    @Override
    @NotNull
    public Integer getServerPort() {
        @Nullable final String propertyPort = properties.getProperty(PropertyConstant.SERVER_PORT);
        @Nullable final String envPort = System.getProperty(PropertyConstant.SERVER_PORT);
        String value = propertyPort;
        if (envPort != null) value = envPort;
        if (value == null) throw new NoSuchPropertyException(PropertyConstant.SERVER_PORT);
        return Integer.parseInt(value);
    }

    @Override
    @NotNull
    public String getSessionSalt() {
        @Nullable final String sessionSalt = properties.getProperty(PropertyConstant.SESSION_SALT);
        if (sessionSalt == null) throw new NoSuchPropertyException(PropertyConstant.SESSION_SALT);
        return sessionSalt;
    }

    @Override
    @NotNull
    public Integer getSessionCycle() {
        @Nullable final String sessionCycle = properties.getProperty(PropertyConstant.SESSION_CYCLE);
        if (sessionCycle == null) throw new NoSuchPropertyException(PropertyConstant.SESSION_CYCLE);
        return Integer.parseInt(sessionCycle);
    }

}
