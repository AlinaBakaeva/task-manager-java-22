package com.bakaeva.tm.endpoint;

import com.bakaeva.tm.api.IServiceLocator;
import com.bakaeva.tm.api.endpoint.IDomainEndpoint;
import com.bakaeva.tm.entity.Session;
import com.bakaeva.tm.enumerated.Role;
import com.bakaeva.tm.exception.security.AccessDeniedException;
import org.jetbrains.annotations.Nullable;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
@AllArgsConstructor
public final class DomainEndpoint implements IDomainEndpoint {

    @NotNull
    private IServiceLocator serviceLocator;

    @Override
    @WebMethod
    public boolean saveToXml(@WebParam(name = "session") @Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getDomainService().saveToXml();
    }

    @Override
    @WebMethod
    public boolean loadFromXml(@WebParam(name = "session") @Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getDomainService().loadFromXml();
    }

    @Override
    @WebMethod
    public boolean removeXml(@WebParam(name = "session") @Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getDomainService().removeXml();
    }

    @Override
    @WebMethod
    public boolean saveToJson(@WebParam(name = "session") @Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getDomainService().saveToJson();
    }

    @Override
    @WebMethod
    public boolean loadFromJson(@WebParam(name = "session") @Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getDomainService().loadFromJson();
    }

    @WebMethod
    public boolean removeJson(@WebParam(name = "session") @Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getDomainService().removeJson();
    }

    @Override
    @WebMethod
    public boolean saveToBinary(@WebParam(name = "session") @Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getDomainService().saveToBinary();
    }

    @Override
    @WebMethod
    public boolean loadFromBinary(@WebParam(name = "session") @Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getDomainService().loadFromBinary();
    }

    @Override
    @WebMethod
    public boolean removeBinary(@WebParam(name = "session") @Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getDomainService().removeBinary();
    }

    @Override
    @WebMethod
    public boolean saveToBase64(@WebParam(name = "session") @Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getDomainService().saveToBase64();
    }

    @Override
    @WebMethod
    public boolean loadFromBase64(@WebParam(name = "session") @Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getDomainService().loadFromBase64();
    }

    @Override
    @WebMethod
    public boolean removeBase64(@WebParam(name = "session") @Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getDomainService().removeBase64();
    }

}
