package com.bakaeva.tm.endpoint;

import com.bakaeva.tm.api.IServiceLocator;
import com.bakaeva.tm.api.endpoint.IUserEndpoint;
import com.bakaeva.tm.entity.Session;
import com.bakaeva.tm.entity.User;
import com.bakaeva.tm.enumerated.Role;
import com.bakaeva.tm.exception.security.AccessDeniedException;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
@AllArgsConstructor
public final class UserEndpoint implements IUserEndpoint {

    @NotNull
    private IServiceLocator serviceLocator;

    @Override
    @Nullable
    @WebMethod
    public User createUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().create(login, password);
    }

    @Override
    @NotNull
    @WebMethod
    public User createUserWithEmail(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "email") @Nullable final String email
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().create(login, password, email);
    }

    @Override
    @NotNull
    @WebMethod
    public User registerUserWithEmail(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "email") @Nullable final String email
    ) {
        return serviceLocator.getUserService().create(login, password, email);
    }

    @Override
    @NotNull
    @WebMethod
    public User createUserWithRole(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "role") @Nullable final Role role
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().create(login, password, role);
    }

    @Override
    @Nullable
    @WebMethod
    public User findUserById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findById(id);
    }

    @Override
    @Nullable
    @WebMethod
    public User findUserByLogin(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findByLogin(login);
    }

    @Override
    @Nullable
    @WebMethod
    public User removeUserById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().removeById(id);
    }

    @Override
    @Nullable
    @WebMethod
    public User removeUserByLogin(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().removeByLogin(login);
    }

    @Override
    @NotNull
    @WebMethod
    public User updateUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName") @Nullable final String lastName,
            @WebParam(name = "middleName") @Nullable final String middleName,
            @WebParam(name = "email") @Nullable final String email
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService()
                .updateById(session.getUserId(), login, firstName, lastName, middleName, email);
    }

    @Override
    @NotNull
    @WebMethod
    public User updateUserById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName") @Nullable final String lastName,
            @WebParam(name = "middleName") @Nullable final String middleName,
            @WebParam(name = "email") @Nullable final String email
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService()
                .updateById(id, login, firstName, lastName, middleName, email);
    }

    @Override
    @NotNull
    @WebMethod
    public User updateUserPassword(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "password") @Nullable final String password
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updatePasswordById(session.getUserId(), password);
    }

    @Override
    @NotNull
    @WebMethod
    public User updateUserPasswordById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "password") @Nullable final String password
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().updatePasswordById(id, password);
    }

    @Override
    @Nullable
    @WebMethod
    public User lockUserByLogin(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().lockUserByLogin(login);
    }

    @Override
    @Nullable
    @WebMethod
    public User unlockUserByLogin(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().unlockUserByLogin(login);
    }

    @Override
    @NotNull
    @WebMethod
    public List<User> findUserAll(@WebParam(name = "session") @Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findAll();
    }
    
    @Override
    @WebMethod
    public void removeUserAll(@WebParam(name = "session") @Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().clear();
    }

    @Override
    @Nullable
    @WebMethod
    public User removeUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "user") @Nullable final User user
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().remove(user);
    }

}