package com.bakaeva.tm.constant;

import org.jetbrains.annotations.NotNull;

public interface PropertyConstant {

    @NotNull
    String FILE_APPLICATION_PROPERTIES = "/application.properties";

    @NotNull
    String SERVER_HOST = "server.host";

    @NotNull
    String SERVER_PORT = "server.port";

    @NotNull
    String SESSION_SALT = "session.salt";

    @NotNull
    String SESSION_CYCLE = "session.cycle";

}
