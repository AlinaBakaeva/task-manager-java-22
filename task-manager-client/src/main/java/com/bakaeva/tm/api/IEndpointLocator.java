package com.bakaeva.tm.api;

import com.bakaeva.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IEndpointLocator {

    @NotNull
    UserEndpoint getUserEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    DomainEndpoint getDomainEndpoint();

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @Nullable
    Session getCurrentSession();

    void setCurrentSession(@Nullable Session session);

}