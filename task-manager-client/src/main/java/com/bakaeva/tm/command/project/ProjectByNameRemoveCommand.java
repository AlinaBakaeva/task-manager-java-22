package com.bakaeva.tm.command.project;

import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.endpoint.Project;
import com.bakaeva.tm.endpoint.Session;
import com.bakaeva.tm.exception.security.AccessDeniedException;
import com.bakaeva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectByNameRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by name.";
    }

    @Override
    public void execute() {
        @Nullable final Session session = endpointLocator.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        @Nullable final Project project = endpointLocator.getProjectEndpoint().removeProjectByName(session, name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }


}