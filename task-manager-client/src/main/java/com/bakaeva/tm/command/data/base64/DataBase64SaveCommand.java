package com.bakaeva.tm.command.data.base64;

import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.endpoint.DomainEndpoint;
import com.bakaeva.tm.endpoint.Session;
import com.bakaeva.tm.exception.security.AccessDeniedException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataBase64SaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-base64-save";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to base64 binary file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA BASE64 SAVE]");
        @Nullable final Session session = endpointLocator.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        @NotNull final DomainEndpoint domainEndpoint = endpointLocator.getDomainEndpoint();
        if (domainEndpoint.saveToBase64(session)) System.out.println("[OK]");
    }

}