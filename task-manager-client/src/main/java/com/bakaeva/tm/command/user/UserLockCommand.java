package com.bakaeva.tm.command.user;

import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.endpoint.Session;
import com.bakaeva.tm.exception.security.AccessDeniedException;
import com.bakaeva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserLockCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-lock";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Lock user by login.";
    }

    @Override
    public void execute() {
        @Nullable final Session session = endpointLocator.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();
        endpointLocator.getUserEndpoint().lockUserByLogin(session, login);
        System.out.println("[OK]");
    }

}